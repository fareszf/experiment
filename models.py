#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Fares Fraij
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# initializing Flask app 
app = Flask(__name__) 

app.app_context().push()

# Change this accordingly 
USER ="postgres"
PASSWORD ="asd123"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="bookdb"



# Configuration 
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING","postgresql://postgres:asd123@localhost:5432/bookdb")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)




# ------------
# Book
# ------------
class Book(db.Model):
    """
    Book class has two attrbiutes 
    title
    id
    """
    __tablename__ = 'book'
	
    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)

if __name__ == "__main__":
    db.drop_all()
    db.create_all()
